<?php

/**
 * Post class.
 */
class ColloquiumPost extends Entity {
  //identifiers
  public $post_id;
  public $topic_id;
  public $forum_id;

  //internal
  protected $topic;
  protected $forum;
  
  /**
  * Retrieves the topic entity the post is attached to
  *
  * Loades the topic entity if its reference wasn't saved into the post entity
  * already, else uses the saved reference.  
  *
  * @return colloquium_topic entity
  */
  public function topic() {
    //check if topic wasn't loaded in the past
    if (empty($this->topic)) {
      //Make sure there is a topic id given
      if (!empty($this->topic_id)) {
        //load topic
        $this->topic = colloquium_topic_load($this->topic_id);
      }
    }
    return $this->topic;
  }
  
  /**
  * Retrieves the forum entity the post belongs to
  *
  * Loades the forum entity if its reference wasn't saved into the post entity
  * already, else uses the saved reference.  
  *
  * @return colloquium_forum entity
  */
  public function forum() {
    //check if forum wasn't loaded in the past
    if (empty($this->forum)) {
      //Make sure there is a topic id given
      if (!empty($this->forum_id)) {
        //load topic
        $this->forum = colloquium_forum_load($this->forum_id);
      }
    }
    return $this->forum;
  }
  
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'cpost/' . $this->identifier());
  }
}