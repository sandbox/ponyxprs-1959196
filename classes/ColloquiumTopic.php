<?php

/**
 * Topic class.
 */
class ColloquiumTopic extends Entity {
  //identifiers
  public $topic_id;
  public $forum_id;
  
  //data
  public $title;
  public $type;
  
  //normalized data
  public $replies_count;
  public $views_count;
  public $first_post_id;
  public $first_author_name;
  public $first_author_id;
  public $first_post_time;
  public $last_post_id;
  public $last_author_name;
  public $last_author_id;
  public $last_post_time;
  
  //internal
  protected $forum;
  protected $first_post;
  protected $last_post;
  
  /**
   * Sets the last post in the topic and updates the normalized data
   * 
   * @param the post entity to set as the last post in the topic
   */
  public function setLastPost($post) {
    $this->last_post = $post;
    
    $this->last_author_id = $post->author_id;
    $this->last_author_name = user_load($post->author_id)->name;
    $this->last_post_time = $post->post_time;
    
    $this->forum()->setLastTopic($this);
    colloquium_forum_save($this->forum());
  }
  
  /**
  * Retrieves the forum entity the topic belongs to
  *
  * Loades the forum entity if its reference wasn't saved into the topic entity
  * already, else uses the saved reference.  
  *
  * @return colloquium_forum entity
  */
  public function forum() {
    //check if forum wasn't loaded in the past
    if (empty($this->forum)) {
      //Make sure there is a topic id given
      if (!empty($this->forum_id)) {
        //load topic
        $this->forum = colloquium_forum_load($this->forum_id);
      }
    }
    return $this->forum;
  }
  
  /**
  * Get a list of colloquium_post ids associated with the topic
  *
  * @return array of colloquium_post ids
  */
  public function getPostIDs() {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'colloquium_post')
          ->propertyCondition('topic_id', $this->topic_id);

    $result = $query->execute();

    $post_ids = array_keys($result['colloquium_post']);

    return $post_ids;
  }
  
  /**
  * Deletes all posts belonging to the topic
  */
  public function purgePosts() {
    $post_ids = $this->getPostIDs();
    colloquium_post_delete_multiple($post_ids, FALSE);
  }
  
  /**
   * Lowers the replies_count for the topic
   * 
   * @param the amount to be subtracted, default 1
   */
  public function decreaseRepliesCount($amount = 1) {
    $this->replies_count = $this->replies_count - $amount;
  }
  
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'ctopic/' . $this->identifier());
  }
}
