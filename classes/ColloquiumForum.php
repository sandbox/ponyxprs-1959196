<?php

/**
 * Forum class.
 */
class ColloquiumForum extends Entity {
  //identifiers
  public $forum_id;
  public $parent_id;
  
  //data
  public $title;
  public $description;
  public $delta;
  
  //normalized data
  public $post_count;
  public $topic_count;
  public $last_author_name;
  public $last_author_id;
  public $last_topic_subject;
  public $last_topic_id;
  public $last_post_time;
  
  //internal
  protected $parent_forum;
  protected $last_topic;
  
  /**
   * Set latest topic for the forum
   * 
   * @param the topic entity to set as the latest topic in the forum
   */
  public function setLastTopic($topic) {
    $last_topic = $topic;
    
    $this->last_author_name = $topic->last_author_name;
    $this->last_author_id = $topic->last_author_id;
    $this->last_topic_subject = $topic->title;
    $this->last_topic_id = $topic->topic_id;
    $this->last_post_time = $topic->last_post_time;
    
    //check if forum has parent, if so, set latest topic there as well
    if ($this->parent_forum()) {
      $this->parent_forum()->setLastTopic($topic);
      colloquium_forum_save($this->parent_forum());
    }
  }
  
  /**
  * Retrieves the forum's parent forum
  *
  * Loades the forum entity if its reference wasn't saved into the forum entity
  * already, else uses the saved reference.  
  *
  * @return colloquium_forum entity or NULL if it has no parent
  */
  public function parent_forum() {
    //check if forum wasn't loaded in the past
    if (empty($this->parent_forum)) {
      //Make sure there is a forum id given and its not 0
      if (!empty($this->parent_id) && $this->parent_id != 0) {
        //load forum
        $this->parent_forum = colloquium_forum_load($this->parent_id);
      }
    }
    return $this->parent_forum;
  }
  
  /**
  * Update the data of the last post directly from the db
  */
  public function updateLastPostData() {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'colloquium_topic')
          ->propertyCondition('forum_id', $this->forum_id)
          ->propertyOrderBy('last_post_time', 'DESC');
    
    $query->range(0,1);
    
    $result = $query->execute();

    $topic_ids = array_keys($result['colloquium_topic']);
    $topic_id = $topic_ids[0];
    
    $topic = colloquium_topic_load($topic_id);
    
    $this->last_author_name = $topic->last_author_name;
    $this->last_author_id = $topic->last_author_id;
    $this->last_topic_subject = $topic->$title;
    $this->last_topic_id = $topic->$topic_id;
    $this->last_post_time = $topic->last_post_time;
  }
  
   /*
   * Rise the topic count of the forum
   */
  public function increaseTopicCount() {
    $this->topic_count = $this->topic_count +1;
    
    //check if forum has parent, if so, increase it's topic count as well
    if ($this->parent_forum()) {
      $this->parent_forum()->increaseTopicCount();
      colloquium_forum_save($this->parent_forum());
    }
  }
  
  /*
   * Decreases the topic count of the forum
   * 
   * @param the amount to be substracted, default 1
   */
  public function decreaseTopicCount($amount = 1) {
    $this->topic_count = $this->topic_count - $amount;
    
    //check if forum has parent, if so, decrease it's topic count as well
    if ($this->parent_forum()) {
      $this->parent_forum()->decreaseTopicCount($amount);
      colloquium_forum_save($this->parent_forum());
    }
  }
  
  /*
   * Decrease the post count of the forum
   * 
   * @param the amount ot be substracted, default 1
   */
  public function decreasePostCount($amount = 1) {
    $this->post_count = $this->post_count - $amount;
    
    //check if forum has parent, if so, decrease it's post count as well
    if ($this->parent_forum()) {
      $this->parent_forum()->decreasePostCount($amount);
      colloquium_forum_save($this->parent_forum());
    }
  }
  
  /*
   * Increase the post count of the forum and its parents by 1
   */
  public function increasePostCount() {
    $this->post_count = $this->post_count + 1;
    
    //check if forum has parent, if so, raise it's post count as well
    if ($this->parent_forum()) {
      $this->parent_forum()->increasePostCount();
      colloquium_forum_save($this->parent_forum());
    }
  }
  
  protected function defaultLabel() {
    return $this->title;
  }

  protected function defaultUri() {
    return array('path' => 'cforum/' . $this->identifier());
  }
}