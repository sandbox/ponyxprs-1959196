<?php

/**
 * Build the forum admin form
 *
 * @return
 *   A form array set for theming by theme_tabledrag_example_parent_form()
 */
function colloquium_forum_admin_form($form_state) {
  
  $form['colloquium_forums']['#tree'] = TRUE;

  //building the initial tree structure for the table

  // Fetch the data from the database, ordered by parent/child/weight.
  $result = colloquium_forum_admin_get_data();

  //create form entry per row
  foreach ($result as $item) {

    $form['colloquium_forums'][$item->forum_id] = array(

      'title' => array(
        '#markup' => $item->title,
      ),
      'forum_id' => array(
        '#type' => 'hidden',
        '#default_value' => $item->forum_id,
      ),
      'parent_id' => array(
        '#type' => 'hidden',
        '#default_value' => $item->parent_id,
      ),
      'delta' => array(
        '#type' => 'weight',
        '#title' => t('Delta'),
        '#default_value' => $item->delta,
        '#delta' => 10,
        '#title-display' => 'invisible',
      ),
      'depth' => array(
        '#type' => 'hidden',
        '#value' => $item->depth,
      ),
    );
  }
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

/**
 * Theme callback for the tabledrag_colloquium_forum_admin_form form.
 */
function theme_colloquium_forum_admin_form($variables) {
  $form = $variables['form'];

  // Initialize the variable which will store our table rows.
  $rows = array();

  // Iterate over each element
  foreach (element_children($form['colloquium_forums']) as $id) {

    //add classes needed for the tabledrag to work
    $form['colloquium_forums'][$id]['delta']['#attributes']['class'] = array('colloquium-forum-weight');
    $form['colloquium_forums'][$id]['forum_id']['#attributes']['class'] = array('colloquium-forum-forum_id');
    $form['colloquium_forums'][$id]['parent_id']['#attributes']['class'] = array('colloquium-forum-parent_id');

    $class = array('draggable');

    // calculate intent based on the depth/nesting of the particular item
    $indent = theme('indentation', array('size' => $form['colloquium_forums'][$id]['depth']['#value']));
    unset($form['colloquium_forums'][$id]['depth']);

    $rows[] = array(
      'data' => array(
        array('data' => $indent . drupal_render($form['colloquium_forums'][$id]['title']), 'class' => 'title'),
        array('data' => drupal_render($form['colloquium_forums'][$id]['delta']), 'class' => 'weight'),
        array('data' => drupal_render($form['colloquium_forums'][$id]['forum_id']), 'class' => 'forum_id'),
        array('data' => drupal_render($form['colloquium_forums'][$id]['parent_id']), 'class' => 'parent_id'),
        array('data' => l(t('edit'), 'admin/structure/colloquium/forum/' . $id . '/edit'), 'class' => 'link_edit'),
        array('data' => l(t('delete'), 'admin/structure/colloquium/forum/' . $id . '/delete'), 'class' => 'link_delete'),
      ),
      // assign the dragable class to all rows
      'class' => $class,
    );
  }

  //Define header
  $header = array(
    array('data' => t('Title'), 'class' => 'title'),
    array('data' => t('Delta'), 'class' => 'weight'),
    array('data' => ('Forum ID'), 'class' => 'forum_id'),
    array('data' => t('Parent ID'), 'class' => 'parent_id'),
    array('data' => t('Operations'), 'class' => 'operations', 'colspan' => 2),
  );
  
  $table_id = 'colloquium-admin-forum-table';
  

  // render tabledrag table for output.
  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => $table_id),
  ));

  // And then render any remaining form elements
  $output .= drupal_render_children($form);

  // call drupal_add_tabledrag() function to add the tabledrag.js to the page
  drupal_add_tabledrag($table_id, 'match', 'parent', 'colloquium-forum-parent_id', 'colloquium-forum-parent_id', 'colloquium-forum-forum_id', FALSE);
  drupal_add_tabledrag($table_id, 'order', 'sibling', 'colloquium-forum-weight', NULL, NULL, TRUE);

  return $output;
}

/**
 * Submit callback for the tabledrag_colloquium_forum_admin_form form.
 */
function colloquium_forum_admin_form_submit($form, &$form_state) {
  foreach ($form_state['values']['colloquium_forums'] as $id => $item) {
    db_query(
      "UPDATE {colloquium_forum} SET delta = :delta, parent_id = :parent_id WHERE forum_id = :forum_id",
      array(':delta' => $item['delta'], ':parent_id' => $item['parent_id'], ':forum_id' => $id)
    );
  }
}

/**
 * Retrives the tree structure from database, and sorts by parent/child/weight.
 */
function colloquium_forum_admin_get_data() {
  // Get all 'root node' items (items with no parents), sorted by weight.
  $rootforums = db_query('SELECT forum_id, title, delta, parent_id
                       FROM {colloquium_forum}
                       WHERE (parent_id = 0)
                       ORDER BY delta ASC');
  // Initialize a variable to store our ordered tree structure.
  $itemtree = array();
  // Depth will be incremented in our _get_tree() function for the first
  // parent item, so we start it at -1.
  $depth = -1;
  // Loop through the root nodes, and add their trees to the array.
  foreach ($rootforums as $parent) {
    colloquium_forum_admin_get_tree($parent, $itemtree, $depth);
  }
  return $itemtree;
}

/**
 * Recursively adds to the $itemtree array, ordered by parent/child/weight.
 */
function colloquium_forum_admin_get_tree($parentitem, &$itemtree = array(), &$depth = 0) {
  // Increase our $depth value by one.
  $depth++;
  // Set the current tree 'depth' for this item, used to calculate indentation.
  $parentitem->depth = $depth;
  // Add the parent item to the tree.
  $itemtree[$parentitem->forum_id] = $parentitem;
  // Retrieve each of the children belonging to this parent.
  $children = db_query('SELECT forum_id, title, delta, parent_id
                      FROM {colloquium_forum}
                      WHERE (parent_id = :parent_id)
                      ORDER BY delta ASC',
                      array(':parent_id' => $parentitem->forum_id));
  foreach ($children as $child) {
    // Make sure this child does not already exist in the tree, to avoid loops.
    if (!in_array($child->forum_id, array_keys($itemtree))) {
      // Add this child's tree to the $itemtree array.
      colloquium_forum_admin_get_tree($child, $itemtree, $depth);
    }
  }
  // Finished processing this tree branch.  Decrease our $depth value by one
  // to represent moving to the next branch.
  $depth--;

  return;
}

/**
 * Add new task page callback.
 */
function colloquium_forum_add() {
  $forum = entity_create('colloquium_forum', array());
  drupal_set_title(t('Create a new Forum'));

  $output = drupal_get_form('colloquium_forum_form', $forum);
  
  return $output;
}

/**
 * Forum Form.
 */
function colloquium_forum_form($form, &$form_state, $forum) {
  $form_state['forum'] = $forum;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $forum->title,
  );
  
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($forum->description) ? $forum->description : '',
  );
  
  $form['delta'] = array(
    '#type' => 'hidden',
    '#default_value' => 0,
  );

  field_attach_form('colloquium_forum', $forum, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Forum'),
    '#submit' => $submit + array('colloquium_forum_form_submit'),
  );

  // Show Delete button if we edit a forum
  $forum_id = entity_id('colloquium_forum' ,$forum);
  if (!empty($forum_id) /*&& colloquium_forum_access('edit', $forum)*/) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('colloquium_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'colloquium_forum_form_validate';

  return $form;
}

function colloquium_forum_form_validate($form, &$form_state) {

}

function colloquium_form_submit_delete($form, &$form_state) {
  $forum = $form_state['forum'];
  $forum_uri = entity_uri('colloquium_forum', $forum);
  $form_state['redirect'] = $forum_uri['path'] . '/delete';
}

/**
 * Forum submit handler.
 */
function colloquium_forum_form_submit($form, &$form_state) {
  $forum = $form_state['forum'];

  entity_form_submit_build_entity('colloquium_forum', $forum, $form, $form_state);

  colloquium_forum_save($forum);

  $forum_uri = entity_uri('colloquium_forum', $forum);

  $form_state['redirect'] = 'admin/structure/colloquium/forum';

  drupal_set_message(t('Forum %title saved.', array('%title' => entity_label('colloquium_forum', $forum))));
}

/*
 * Delete confirmation form.
 */
function colloquium_forum_delete_form($form, &$form_state, $forum) {
  $form_state['forum'] = $forum;
  
  // Always provide entity id in the same form key as in the entity edit form.
  $form['forum_id'] = array('#type' => 'value', '#value' => entity_id('colloquium' ,$forum));
  $forum_uri = entity_uri('colloquium_forum', $forum);
  
  return confirm_form($form,
    t('Are you sure you want to delete the forum %title?', array('%title' => entity_label('colloquium_forum', $forum))),
    $forum_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function colloquium_forum_delete_form_submit($form, &$form_state) {
  $forum = $form_state['forum'];
  colloquium_forum_delete($forum);

  drupal_set_message(t('Forum %title deleted.', array('%title' => entity_label('colloquium_forum', $forum))));

  $form_state['redirect'] = 'admin/structure/colloquium/forum';
}

function colloquium_forum_admin_menu() {
  $content = array(
    'link list' => array(
      'items' => array(
        'forum' => array(
          'title' => array(
            '#markup' => l(t('Forums'), 'admin/structure/colloquium/forum'),
            '#prefix' => '<dt>',
            '#suffix' => '</dt>',
          ),
          'description' => array(
            '#markup' => 'Manage your forums by rearranging their order and adjusting their fields.',
            '#prefix' => '<dd>',
            '#suffix' => '</dd>',
          ),
        ),
        'topic' => array(
          'title' => array(
            '#markup' => l(t('Topic Types'), 'admin/structure/colloquium/topic_types'),
            '#prefix' => '<dt>',
            '#suffix' => '</dt>',
          ),
          'description' => array(
            '#markup' => 'Manage your topics and add additional fields.',
            '#prefix' => '<dd>',
            '#suffix' => '</dd>',
          ),
        ),
        'post' => array(
          'title' => array(
            '#markup' => l(t('Post Types'), 'admin/structure/colloquium/post_types'),
            '#prefix' => '<dt>',
            '#suffix' => '</dt>',
          ),
          'description' => array(
            '#markup' => 'Manage your posts and add additional fields.',
            '#prefix' => '<dd>',
            '#suffix' => '</dd>',
          ),
        ),
      ),
      '#prefix' => '<dl>',
      '#suffix' => '</dl>',
    )
  );
  
  return $content;
}
