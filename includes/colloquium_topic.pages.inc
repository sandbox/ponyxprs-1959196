<?php

/**
 * Topic add page callback.
 */
function colloquium_topic_add($forum, $topic_type) {
  drupal_set_title(t('New topic'));
  
  $topic = entity_create('colloquium_topic', array('type' => $topic_type));
  $topic->forum_id = $forum->forum_id;
  
  if($topic->first_post_id != 0) {
    $first_post = colloquium_post_load($topic->first_post_id);
  } else {
    $topic_type = colloquium_topic_type_load($topic->type);
    $first_post = entity_create('colloquium_post', array('type' => $topic_type->post_type));
  }

  $output = drupal_get_form('colloquium_topic_form', $topic, $first_post);
  
  return $output;
}

/**
 * Topic Form.
 */
function colloquium_topic_form($form, &$form_state, $topic, $first_post) {
  $form_state['topic'] = $topic;
  $form_state['post'] = $first_post;

  $form['topic']['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $topic->title,
  );
  
  $form['topic']['forum_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $topic->forum_id,
  );
  
  $form['post']['forum_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $topic->forum_id,
  );
  
  $form['post']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#required' => TRUE,
    '#default_value' => $first_post->body,
  );

  field_attach_form('colloquium_topic', $topic, $form['topic'], $form_state);
  field_attach_form('colloquium_post', $first_post, $form['post'], $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Topic'),
    '#submit' => $submit + array('colloquium_topic_form_submit'),
  );

  // Show Delete button if we edit a forum
  /*$forum_id = entity_id('colloquium_forum' ,$forum);
  if (!empty($forum_id) && colloquium_forum_access('edit', $forum)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('colloquium_form_submit_delete'),
    );
  }*/

  $form['#validate'][] = 'colloquium_topic_form_validate';

  return $form;
}

function colloquium_topic_form_validate($form, &$form_state) {

}

/**
 * Topic submit handler.
 */
function colloquium_topic_form_submit($form, &$form_state) {
  $topic = $form_state['topic'];
  $first_post = $form_state['post'];

  entity_form_submit_build_entity('colloquium_topic', $topic, $form['topic'], $form_state);
  entity_form_submit_build_entity('colloquium_post', $first_post, $form['post'], $form_state);
  
  colloquium_topic_save($topic);
  
  //if the thread was saved the first time, give its id to the first post
  if ($first_post->topic_id == 0) {
    $first_post->topic_id = $topic->topic_id;
    
    //synch creation time
    $first_post->post_time = $topic->first_post_time;
  }
  
  colloquium_post_save($first_post);

  $form_state['redirect'] = 'cforum/' . $topic->forum_id;

  drupal_set_message(t('Topic %title saved.', array('%title' => entity_label('colloquium_topic', $topic))));
}

/**
 * Topic reply page callback.
 */
function colloquium_topic_reply($forum, $topic) {
  $topic_type = colloquium_topic_type_load($topic->type);
  $post = entity_create('colloquium_post', array('type' => $topic_type->post_type));
  $post->forum_id = $forum->forum_id;
  $post->topic_id = $topic->topic_id;
  drupal_set_title(t('Add Reply'));

  $output = drupal_get_form('colloquium_post_form', $post);
  
  return $output;
}

/**
 * Post Form.
 */
function colloquium_post_form($form, &$form_state, $post) {
  $form_state['post'] = $post;
  
  $form['topic_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $post->topic_id,
  );
  
  $form['forum_id'] = array(
    '#type' => 'hidden',
    '#default_value' => $post->forum_id,
  );
  
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $post->body,
  );

  field_attach_form('colloquium_post', $post, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Post'),
    '#submit' => $submit + array('colloquium_post_form_submit'),
  );

  // Show Delete button if we edit a forum
  /*$forum_id = entity_id('colloquium_forum' ,$forum);
  if (!empty($forum_id) && colloquium_forum_access('edit', $forum)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('colloquium_form_submit_delete'),
    );
  }*/

  $form['#validate'][] = 'colloquium_post_form_validate';

  return $form;
}

function colloquium_post_form_validate($form, &$form_state) {

}

/**
 * Topic submit handler.
 */
function colloquium_post_form_submit($form, &$form_state) {
  $post = $form_state['post'];

  entity_form_submit_build_entity('colloquium_post', $post, $form, $form_state);
  
  colloquium_post_save($post);

  $form_state['redirect'] = 'cforum/' . $post->forum_id; //TODO: get proper redirect path

  drupal_set_message(t('Post saved.'));
}

/**
 * Topic edit page callback.
 */
function colloquium_post_edit_page($forum, $topic, $post) {
  drupal_set_title(t('Edit Reply'));
  
  //check if the topic should be edited as well
  if($topic->first_post_id == $post->post_id) {
    $output = drupal_get_form('colloquium_topic_form', $topic, $post);
  //else edit just the post
  } else {
    $output = drupal_get_form('colloquium_post_form', $post);
  }
  
  return $output;
}

/**
 * Post delete page callback.
 */
function colloquium_post_delete_page($forum, $topic, $post) {
  //check if the topic should be deleted as well
  if($topic->first_post_id == $post->post_id) {
    drupal_set_title(t('Delete Topic'));
    $output = drupal_get_form('colloquium_topic_delete_form', $topic);
  //else edit just the post
  } else {
    drupal_set_title(t('Delete Reply'));
    $output = drupal_get_form('colloquium_post_delete_form', $post);
  }
  
  return $output;
}

/*
 * Delete confirmation form for deleting posts.
 */
function colloquium_post_delete_form($form, &$form_state, $post) {
  $form_state['post'] = $post;
  
  // Always provide entity id in the same form key as in the entity edit form.
  $form['post_id'] = array('#type' => 'value', '#value' => entity_id('colloquium_post' ,$post));
  $post_uri = entity_uri('colloquium_post', $post);
  
  return confirm_form($form,
    t('Are you sure you want to delete this post?'),
    $post_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler for deleting posts.
 */
function colloquium_post_delete_form_submit($form, &$form_state) {
  $post = $form_state['post'];
  $parent_forum = $post->forum_id;
  $parent_topic = $post->topic_id;
  
  colloquium_post_delete($post);

  drupal_set_message(t('Post successfully deleted'));

  $form_state['redirect'] = 'cforum/' . $parent_forum . '/ctopic/' . $parent_topic;
}

/*
 * Delete confirmation form for deleting topics.
 */
function colloquium_topic_delete_form($form, &$form_state, $topic) {
  $form_state['topic'] = $topic;
  
  // Always provide entity id in the same form key as in the entity edit form.
  $form['topic_id'] = array('#type' => 'value', '#value' => entity_id('colloquium_topic' ,$topic));
  
  //TODO: set proper path
  $forum_uri = entity_uri('colloquium_topic', $topic);
  
  return confirm_form($form,
    t('Are you sure you want to delete this topic?'),
    $forum_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler for deleting posts.
 */
function colloquium_topic_delete_form_submit($form, &$form_state) {
  $topic = $form_state['topic'];
  $parent_forum = $topic->forum_id;

  colloquium_topic_delete($topic);

  drupal_set_message(t('Topic successfully deleted'));

  $form_state['redirect'] = 'cforum/' . $parent_forum;
}