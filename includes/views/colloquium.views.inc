<?php

function colloquium_views_data_alter(&$data){
  
  $data['colloquium_post']['post_link'] = array(
    'title' => t('Link'), 
    'help' => t('Displays a simple link to the post'), 
    'field' => array(
      'handler' => 'colloquium_handler_field_post_link',
    ),
  );
  
  $data['colloquium_post']['post_link_edit'] = array(
    'title' => t('Edit Link'), 
    'help' => t('Displays a simple link to edit post'), 
    'field' => array(
      'handler' => 'colloquium_handler_field_post_link_edit',
    ),
  );
  
  $data['colloquium_post']['post_link_delete'] = array(
    'title' => t('Delete Link'), 
    'help' => t('Displays a simple link to delete a post or incase of the first post the topic'), 
    'field' => array(
      'handler' => 'colloquium_handler_field_post_link_delete',
    ),
  );
  
  $data['colloquium_topic']['topic_link'] = array(
    'title' => t('Link'), 
    'help' => t('Displays a simple link to add new topics per type'), 
    'area' => array(
      'handler' => 'colloquium_handler_area_topic_link_add',
    ),
  );
}
