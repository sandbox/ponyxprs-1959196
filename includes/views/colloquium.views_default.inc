<?php

/**
 * @file
 * Creates default views for the colloquium module.
 */

/**
 * Implements hook_views_default_views().
 */
function colloquium_views_default_views() {
  $views = array();
  
  //forum view
  $view = new view();
  $view->name = 'forum';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'colloquium_forum';
  $view->human_name = 'Forum';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Subforum';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'forum_id' => 'forum_id',
    'description' => 'title',
    'title' => 'title',
    'topic_count' => 'topic_count',
    'post_count' => 'post_count',
    'last_post_time' => 'last_post_time',
    'last_topic_subject' => 'last_topic_subject',
    'last_author_id' => 'last_author_id',
    'last_author_name' => 'last_author_name',
    'title_1' => 'title_1',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'forum_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'description' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br>',
      'empty_column' => 0,
    ),
    'topic_count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'post_count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_post_time' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_topic_subject' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_author_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_author_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => 'views-align-right',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title_1' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Global: View area */
  $handler->display->display_options['footer']['view']['id'] = 'view';
  $handler->display->display_options['footer']['view']['table'] = 'views';
  $handler->display->display_options['footer']['view']['field'] = 'view';
  $handler->display->display_options['footer']['view']['view_to_insert'] = 'topic_listing:attachment_1';
  $handler->display->display_options['footer']['view']['inherit_arguments'] = TRUE;
  /* Relationship: Colloquium Forum: Parent Forum ID */
  $handler->display->display_options['relationships']['parent_id']['id'] = 'parent_id';
  $handler->display->display_options['relationships']['parent_id']['table'] = 'colloquium_forum';
  $handler->display->display_options['relationships']['parent_id']['field'] = 'parent_id';
  $handler->display->display_options['relationships']['parent_id']['label'] = 'Parent Forum';
  /* Field: Colloquium Forum: Forum ID */
  $handler->display->display_options['fields']['forum_id']['id'] = 'forum_id';
  $handler->display->display_options['fields']['forum_id']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['forum_id']['field'] = 'forum_id';
  $handler->display->display_options['fields']['forum_id']['label'] = '';
  $handler->display->display_options['fields']['forum_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['forum_id']['element_label_colon'] = FALSE;
  /* Field: Colloquium Forum: Forum Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Forum';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'cforum/[forum_id]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = TRUE;
  /* Field: Colloquium Forum: Forum Description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = '';
  $handler->display->display_options['fields']['description']['element_label_colon'] = FALSE;
  /* Field: Colloquium Forum: Total Topic Number */
  $handler->display->display_options['fields']['topic_count']['id'] = 'topic_count';
  $handler->display->display_options['fields']['topic_count']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['topic_count']['field'] = 'topic_count';
  $handler->display->display_options['fields']['topic_count']['label'] = 'Topics';
  $handler->display->display_options['fields']['topic_count']['format_plural_singular'] = '1 Topic';
  $handler->display->display_options['fields']['topic_count']['format_plural_plural'] = '@count Topics';
  /* Field: Colloquium Forum: Total Post Number */
  $handler->display->display_options['fields']['post_count']['id'] = 'post_count';
  $handler->display->display_options['fields']['post_count']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['post_count']['field'] = 'post_count';
  $handler->display->display_options['fields']['post_count']['label'] = 'Posts';
  $handler->display->display_options['fields']['post_count']['format_plural_singular'] = '1 Post';
  $handler->display->display_options['fields']['post_count']['format_plural_plural'] = '@count Posts';
  /* Field: Colloquium Forum: Last Post Date */
  $handler->display->display_options['fields']['last_post_time']['id'] = 'last_post_time';
  $handler->display->display_options['fields']['last_post_time']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['last_post_time']['field'] = 'last_post_time';
  $handler->display->display_options['fields']['last_post_time']['label'] = '';
  $handler->display->display_options['fields']['last_post_time']['exclude'] = TRUE;
  $handler->display->display_options['fields']['last_post_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['last_post_time']['date_format'] = 'long';
  /* Field: Colloquium Forum: Most Recent Topic */
  $handler->display->display_options['fields']['last_topic_subject']['id'] = 'last_topic_subject';
  $handler->display->display_options['fields']['last_topic_subject']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['last_topic_subject']['field'] = 'last_topic_subject';
  $handler->display->display_options['fields']['last_topic_subject']['label'] = '';
  $handler->display->display_options['fields']['last_topic_subject']['exclude'] = TRUE;
  $handler->display->display_options['fields']['last_topic_subject']['element_label_colon'] = FALSE;
  /* Field: Colloquium Forum: Last Active Author ID */
  $handler->display->display_options['fields']['last_author_id']['id'] = 'last_author_id';
  $handler->display->display_options['fields']['last_author_id']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['last_author_id']['field'] = 'last_author_id';
  $handler->display->display_options['fields']['last_author_id']['label'] = '';
  $handler->display->display_options['fields']['last_author_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['last_author_id']['element_label_colon'] = FALSE;
  /* Field: Colloquium Forum: Last Active Author */
  $handler->display->display_options['fields']['last_author_name']['id'] = 'last_author_name';
  $handler->display->display_options['fields']['last_author_name']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['last_author_name']['field'] = 'last_author_name';
  $handler->display->display_options['fields']['last_author_name']['label'] = 'Last Activity';
  $handler->display->display_options['fields']['last_author_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['last_author_name']['alter']['text'] = 'Topic: [last_topic_subject] by <a href="user/[last_author_id]">[last_author_name]</a><br/>
  on [last_post_time]';
  $handler->display->display_options['fields']['last_author_name']['empty'] = 'never';
  /* Field: Colloquium Forum: Forum Title */
  $handler->display->display_options['fields']['title_1']['id'] = 'title_1';
  $handler->display->display_options['fields']['title_1']['table'] = 'colloquium_forum';
  $handler->display->display_options['fields']['title_1']['field'] = 'title';
  $handler->display->display_options['fields']['title_1']['relationship'] = 'parent_id';
  $handler->display->display_options['fields']['title_1']['label'] = '';
  $handler->display->display_options['fields']['title_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title_1']['empty'] = 'Forum';
  $handler->display->display_options['fields']['title_1']['empty_zero'] = TRUE;
  /* Sort criterion: Colloquium Forum: Forum Delta */
  $handler->display->display_options['sorts']['delta']['id'] = 'delta';
  $handler->display->display_options['sorts']['delta']['table'] = 'colloquium_forum';
  $handler->display->display_options['sorts']['delta']['field'] = 'delta';
  /* Contextual filter: Colloquium Forum: Parent Forum ID */
  $handler->display->display_options['arguments']['parent_id']['id'] = 'parent_id';
  $handler->display->display_options['arguments']['parent_id']['table'] = 'colloquium_forum';
  $handler->display->display_options['arguments']['parent_id']['field'] = 'parent_id';
  $handler->display->display_options['arguments']['parent_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['parent_id']['breadcrumb'] = 'Forum';
  $handler->display->display_options['arguments']['parent_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['parent_id']['default_argument_options']['argument'] = '0';
  $handler->display->display_options['arguments']['parent_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['parent_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['parent_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['parent_id']['validate']['type'] = 'numeric';
  
  /* Display: Front Page */
  $handler = $view->new_display('page', 'Front Page', 'page');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Forum';
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['path'] = 'cforum';
  
  /* Display: Subforum */
  $handler = $view->new_display('page', 'Subforum', 'page_1');
  $handler->display->display_options['path'] = 'cforum/%';
  
    // Add view to list of views to provide if it doesn't already exist.
  if (!views_get_view($view->name)) {
    $views[$view->name] = $view;
  }
  
  
  //topic listings
  $view = new view();
  $view->name = 'topic_listing';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'colloquium_topic';
  $view->human_name = 'Topic Listing';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'topic_id' => 'topic_id',
    'title' => 'title',
    'first_post_time' => 'first_post_time',
    'first_author_name' => 'title',
    'replies_count' => 'replies_count',
    'views_count' => 'views_count',
    'last_post_time' => 'last_post_time',
    'last_author_name' => 'last_author_name',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'topic_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br>',
      'empty_column' => 0,
    ),
    'first_post_time' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'first_author_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'replies_count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'views_count' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_post_time' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'last_author_name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Colloquium Topic: Link */
  $handler->display->display_options['footer']['topic_link']['id'] = 'topic_link';
  $handler->display->display_options['footer']['topic_link']['table'] = 'colloquium_topic';
  $handler->display->display_options['footer']['topic_link']['field'] = 'topic_link';
  $handler->display->display_options['footer']['topic_link']['forum_id'] = '!1';
  $handler->display->display_options['footer']['topic_link']['tokenize'] = TRUE;
  /* Field: Colloquium Topic: Topic ID */
  $handler->display->display_options['fields']['topic_id']['id'] = 'topic_id';
  $handler->display->display_options['fields']['topic_id']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['topic_id']['field'] = 'topic_id';
  $handler->display->display_options['fields']['topic_id']['label'] = '';
  $handler->display->display_options['fields']['topic_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['topic_id']['element_label_colon'] = FALSE;
  /* Field: Colloquium Topic: Topic Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Topic';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = 'cforum/!1/ctopic/[topic_id]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = TRUE;
  /* Field: Colloquium Topic: First Post Date */
  $handler->display->display_options['fields']['first_post_time']['id'] = 'first_post_time';
  $handler->display->display_options['fields']['first_post_time']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['first_post_time']['field'] = 'first_post_time';
  $handler->display->display_options['fields']['first_post_time']['label'] = '';
  $handler->display->display_options['fields']['first_post_time']['exclude'] = TRUE;
  $handler->display->display_options['fields']['first_post_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['first_post_time']['empty'] = 'Sun, 15th. July 2013';
  $handler->display->display_options['fields']['first_post_time']['date_format'] = 'medium';
  /* Field: Colloquium Topic: First Author */
  $handler->display->display_options['fields']['first_author_name']['id'] = 'first_author_name';
  $handler->display->display_options['fields']['first_author_name']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['first_author_name']['field'] = 'first_author_name';
  $handler->display->display_options['fields']['first_author_name']['label'] = '';
  $handler->display->display_options['fields']['first_author_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['first_author_name']['alter']['text'] = 'by [first_author_name] ([first_post_time])';
  $handler->display->display_options['fields']['first_author_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['first_author_name']['empty'] = 'Test User';
  /* Field: Colloquium Topic: Replies Count. */
  $handler->display->display_options['fields']['replies_count']['id'] = 'replies_count';
  $handler->display->display_options['fields']['replies_count']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['replies_count']['field'] = 'replies_count';
  $handler->display->display_options['fields']['replies_count']['label'] = 'Replies';
  /* Field: Colloquium Topic: Views Count. */
  $handler->display->display_options['fields']['views_count']['id'] = 'views_count';
  $handler->display->display_options['fields']['views_count']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['views_count']['field'] = 'views_count';
  $handler->display->display_options['fields']['views_count']['label'] = 'Views';
  /* Field: Colloquium Topic: Last Post Date */
  $handler->display->display_options['fields']['last_post_time']['id'] = 'last_post_time';
  $handler->display->display_options['fields']['last_post_time']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['last_post_time']['field'] = 'last_post_time';
  $handler->display->display_options['fields']['last_post_time']['label'] = '';
  $handler->display->display_options['fields']['last_post_time']['exclude'] = TRUE;
  $handler->display->display_options['fields']['last_post_time']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['last_post_time']['empty'] = 'Mon, 16th. July 2013';
  $handler->display->display_options['fields']['last_post_time']['date_format'] = 'medium';
  /* Field: Colloquium Topic: Last Author */
  $handler->display->display_options['fields']['last_author_name']['id'] = 'last_author_name';
  $handler->display->display_options['fields']['last_author_name']['table'] = 'colloquium_topic';
  $handler->display->display_options['fields']['last_author_name']['field'] = 'last_author_name';
  $handler->display->display_options['fields']['last_author_name']['label'] = 'Last Post';
  $handler->display->display_options['fields']['last_author_name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['last_author_name']['alter']['text'] = 'by [last_author_name]<br/>
  ([last_post_time])';
  $handler->display->display_options['fields']['last_author_name']['empty'] = 'Test User 2';
  /* Sort criterion: Colloquium Topic: Last Post Date */
  $handler->display->display_options['sorts']['last_post_time']['id'] = 'last_post_time';
  $handler->display->display_options['sorts']['last_post_time']['table'] = 'colloquium_topic';
  $handler->display->display_options['sorts']['last_post_time']['field'] = 'last_post_time';
  $handler->display->display_options['sorts']['last_post_time']['order'] = 'DESC';
  /* Contextual filter: Colloquium Topic: Parent Forum ID */
  $handler->display->display_options['arguments']['forum_id']['id'] = 'forum_id';
  $handler->display->display_options['arguments']['forum_id']['table'] = 'colloquium_topic';
  $handler->display->display_options['arguments']['forum_id']['field'] = 'forum_id';
  $handler->display->display_options['arguments']['forum_id']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['forum_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['forum_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['forum_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['forum_id']['summary_options']['items_per_page'] = '25';
  
  /* Display: Attachment */
  $handler = $view->new_display('attachment', 'Attachment', 'attachment_1');
  $handler->display->display_options['pager']['type'] = 'some';
  
  // Add view to list of views to provide if it doesn't already exist.
  if (!views_get_view($view->name)) {
    $views[$view->name] = $view;
  }
  
  //Topic 
  $view = new view();
  $view->name = 'topic';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'colloquium_post';
  $view->human_name = 'Topic';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Topic';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'picture' => 'name',
    'created' => 'name',
    'access' => 'name',
    'body' => 'body',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '<br>',
      'empty_column' => 0,
    ),
    'picture' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'access' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'body' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['content'] = '<a href="!2/reply">Add a reply</a>';
  $handler->display->display_options['footer']['area']['format'] = 'filtered_html';
  $handler->display->display_options['footer']['area']['tokenize'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'Ne Result Text';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No posts - shouldn\'t happen~';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Colloquium Post: Author ID */
  $handler->display->display_options['relationships']['author_id']['id'] = 'author_id';
  $handler->display->display_options['relationships']['author_id']['table'] = 'colloquium_post';
  $handler->display->display_options['relationships']['author_id']['field'] = 'author_id';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'author_id';
  $handler->display->display_options['fields']['name']['label'] = 'User';
  /* Field: User: Picture */
  $handler->display->display_options['fields']['picture']['id'] = 'picture';
  $handler->display->display_options['fields']['picture']['table'] = 'users';
  $handler->display->display_options['fields']['picture']['field'] = 'picture';
  /* Field: User: Created date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'users';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['relationship'] = 'author_id';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['created']['alter']['text'] = 'Member since: [created]';
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'd.m.Y';
  /* Field: User: Last access */
  $handler->display->display_options['fields']['access']['id'] = 'access';
  $handler->display->display_options['fields']['access']['table'] = 'users';
  $handler->display->display_options['fields']['access']['field'] = 'access';
  $handler->display->display_options['fields']['access']['relationship'] = 'author_id';
  $handler->display->display_options['fields']['access']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['access']['alter']['text'] = 'Last Access: [access]';
  $handler->display->display_options['fields']['access']['date_format'] = 'custom';
  $handler->display->display_options['fields']['access']['custom_date_format'] = 'd.m.Y';
  /* Field: Colloquium Post: Post Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'colloquium_post';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Body';
  /* Contextual filter: Colloquium Post: Parent Forum ID */
  $handler->display->display_options['arguments']['forum_id']['id'] = 'forum_id';
  $handler->display->display_options['arguments']['forum_id']['table'] = 'colloquium_post';
  $handler->display->display_options['arguments']['forum_id']['field'] = 'forum_id';
  $handler->display->display_options['arguments']['forum_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['forum_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['forum_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['forum_id']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Colloquium Post: Parent Topic ID */
  $handler->display->display_options['arguments']['topic_id']['id'] = 'topic_id';
  $handler->display->display_options['arguments']['topic_id']['table'] = 'colloquium_post';
  $handler->display->display_options['arguments']['topic_id']['field'] = 'topic_id';
  $handler->display->display_options['arguments']['topic_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['topic_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['topic_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['topic_id']['summary_options']['items_per_page'] = '25';
  
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'cforum/%/ctopic/%';
  
    // Add view to list of views to provide if it doesn't already exist.
  if (!views_get_view($view->name)) {
    $views[$view->name] = $view;
  }
  
  return $views;
}