<?php

/**
 * @file
 * Definition of colloquium_handler_field_post_link_edit.
 */

/**
 * Base field handler to present a link to edit a post.
 *
 * @ingroup colloquium_field_handlers
 */
class colloquium_handler_field_post_link_edit extends colloquium_handler_field_post_link {
  function construct() {
    parent::construct();
  }

  function render_link($post_id, $topic_id, $forum_id, $values) {
    //make sure user is allowed to edit posts
    global $user;
    $post_author = $this->sanitize_value($this->get_value($values, 'author_id'));
    
    //TODO: add proper access permissions, this is just a work around
    if($user->uid != $post_author && !in_array('administrator', $user->roles)) {
      return;
    }
    
    $text = !empty($this->options['text']) ? $this->options['text'] : t('edit');

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['html'] = TRUE;


    $this->options['alter']['path'] = "cforum/" . $forum_id . "/ctopic/" . $topic_id . "/cpost/" . $post_id . "/edit";

    return $text;
  }
}
