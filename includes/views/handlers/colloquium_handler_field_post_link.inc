<?php

/**
 * @file
 * Definition of colloquium_handler_field_post_link.
 */

/**
 * Base field handler to present a link.
 *
 * @ingroup colloquium_field_handlers
 */
class colloquium_handler_field_post_link extends views_handler_field_entity {
  function construct() {
    parent::construct();
    
    $this->additional_fields['post_id'] = 'post_id';
    $this->additional_fields['topic_id'] = 'topic_id';
    $this->additional_fields['forum_id'] = 'forum_id';
    $this->additional_fields['author_id'] = 'author_id';
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['text'] = array('default' => '', 'translatable' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['text'] = array(
      '#type' => 'textfield',
      '#title' => t('Text to display'),
      '#default_value' => $this->options['text'],
    );

    parent::options_form($form, $form_state);
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $post_id = $this->sanitize_value($this->get_value($values, 'post_id'));
    $topic_id = $this->sanitize_value($this->get_value($values, 'topic_id'));
    $forum_id = $this->sanitize_value($this->get_value($values, 'forum_id'));
    
    return $this->render_link($post_id, $topic_id, $forum_id , $values);

  }

  function render_link($post_id, $topic_id, $forum_id , $values) {
    $text = !empty($this->options['text']) ? $this->options['text'] : t('view');

    $this->options['alter']['make_link'] = TRUE;
    $this->options['alter']['html'] = TRUE;


    $this->options['alter']['path'] = "cpost/" . $post_id;

    return $text;
  }
}
