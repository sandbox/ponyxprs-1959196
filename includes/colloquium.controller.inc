<?php

class ColloquiumForumController extends EntityAPIController{
  
  public function create(array $values = array()) {
    //set default values for creation
    $values += array(
      'parent_id' => 0,
      'title' => '',
      'delta' => 0,
      'topic_count' => 0,
      'post_count' => 0,
    );
    return parent::create($values);
  }
  
  //build content for display, when not using views - might not be needed
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('colloquium_forum', $entity);
    
    $content['post_count'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Post count'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'post count',
      '#field_type' => 'number_integer',
      '#entity_type' => 'colloquium_forum',
      '#bundle' => 'colloquium_forum',
      '#items' => array(array('value' => $entity->post_count)),
      '#formatter' => 'number_integer',
      0 => array('#markup' => check_plain($entity->post_count))
    );
    
    $content['topic_count'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Topic count'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'topic count',
      '#field_type' => 'number_integer',
      '#entity_type' => 'colloquium_forum',
      '#bundle' => 'colloquium_forum',
      '#items' => array(array('value' => $entity->topic_count)),
      '#formatter' => 'number_integer',
      0 => array('#markup' => check_plain($entity->topic_count))
    );
    
    $content['last_author_name'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Last Poster'),
      '#access' => TRUE,
      '#label_display' => 'inline',
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'last author',
      '#field_type' => 'text',
      '#entity_type' => 'colloquium_forum',
      '#bundle' => 'colloquium_forum',
      '#items' => array(array('value' => $entity->last_author_name)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->last_author_name))
    );
    
    $content['last_topic_subject'] = array(
      '#theme' => 'field',
      '#weight' => 0,
      '#title' =>t('Last Post'),
      '#access' => TRUE,
      '#label_display' => 'above',
      '#view_mode' => $view_mode,
      '#language' => LANGUAGE_NONE,
      '#field_name' => 'last post',
      '#field_type' => 'text',
      '#entity_type' => 'colloquium_forum',
      '#bundle' => 'colloquium_forum',
      '#items' => array(array('value' => $entity->last_topic_subject)),
      '#formatter' => 'text_default',
      0 => array('#markup' => check_plain($entity->last_topic_subject))
    );
    
    //TODO: check if label for output corresponds with those for display managemant in hook_field_extra_fields

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

class ColloquiumTopicController extends EntityAPIController{
  public function create(array $values = array()) {
    global $user;
    
    //set defaults
    $values += array(
      'forum_id' => 0,
      'title' => '',
      'type' => 0,
      'status' => 0,
      'views_count' => 0,
      'replies_count' => 0,
      'first_post_id' => 0,
      'first_author_name' => $user->name,
      'first_author_id' => $user->uid,
      'first_post_time' => REQUEST_TIME,
      'last_post_id' => 0,
      'last_author_name' => $user->name,
      'last_author_id' => $user->uid,
      'last_post_time' => REQUEST_TIME,
    );
    return parent::create($values);
  }
  
  /**
  * Deletes entities and applies updates to the forum it belonged to
  */
  public function delete($ids, DatabaseTransaction $transaction = NULL) {
    $topics = array(); //array of topics keyed by forum_id
    
    //load topics and group them by forum_id
    foreach ($ids as $id) {
      $topic = colloquium_topic_load($id);
      $topics[$topic->forum_id][$topic->topic_id] = $topic;
    }
    
    //update on forum basis
    foreach ($topics as $forum_id => $topics_by_forum) {
      //load forum
      $forum = colloquium_forum_load($forum_id);
      
      //decrease topic count of the forum
      $forum->decreaseTopicCount(count($topics_by_forum));
      
      //update on topic basis
      foreach ($topics_by_forum as $topic_id => $topic) {
      
        //decrease post count of the forum based on replies in the topic + 1 for the first post
        $forum->decreasePostCount($topic->replies_count + 1);
      
        //if deleted topic was the newest in the forum
        if ($forum->last_topic_id == $topic->topic_id) {
          //TODO: find newest topic and save info to parent forum if the deleted forum was the most recent
        }
        
        //delete all posts belonging to the topic
        $topic->purgePosts();
      }
      
      //save forum
      colloquium_forum_save($forum);
      
    }
    
    parent::delete($ids, $transaction = NULL);
  }
  
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('colloquium_forum', $entity);

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

class ColloquiumTopicTypeController extends EntityAPIControllerExportable {
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'button_label' => 'Topic',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Topic Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Topic Type.
 */
class ColloquiumTopicTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  /*public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage topic types.';
    return $items;
  }*/
}

class ColloquiumPostController extends EntityAPIController{
  public function create(array $values = array()) {
    global $user;
    
    $values += array(
      'topic_id' => 0,
      'forum_id' => 0,
      'author_id' => $user->uid,
      'body' => '',
      'post_time' => REQUEST_TIME,
      'author_ip' => $_SERVER['REMOTE_ADDR'],
    );
    return parent::create($values);
  }
  
  /**
  * updates associated topic and forum entities TODO: needs batch delete for many posts ???
  */
  public function prepare_deletion($ids) {
    $posts = array(); //array of posts keyed by forum_id and topic_id
    
    //load posts and group them by forum_id and topic_id
    foreach($ids as $id) {
      $post = colloquium_post_load($id);
      $posts[$post->forum_id][$post->topic_id][$post->post_id] = $post;
    }
    
    //updates on forum level
    foreach($posts as $forum_id => $posts_by_forum) {
      $forum_post_count = 0;
      
      //updates on topic level
      foreach($posts_by_forum as $topic_id => $posts_by_topic) {
        //load topic
        $topic = colloquium_topic_load($topic_id);
        
        //get topic count
        $topic_post_count = count($posts_by_topic);
        
        //if there are as many replies counted as noted on the topic, the topic gets
        //deleted (as there should not be any topics without at least 1 post), 
        //so don't bother with further updates
        if ($topic_post_count - 1 != $topic->replies_count) {
            
          //reduces the replies count of the parent forum
          $topic->decreaseRepliesCount($topic_post_count);
          
          //updates on post level
          foreach($posts_by_topic as $post_id => $post) {
            //TODO: check if post was the last in the topic -> if so update last post activity on parent topic
          }
        
        }
        
        //save topic
        colloquium_topic_save($topic);
        
        //add post count to the post count of the forum
        $forum_post_count += $topic_post_count;
      }
      
      //load forum
      $forum = colloquium_forum_load($forum_id);
      
      //decrease post count
      $forum->decreasePostCount($forum_post_count);
      
      //save forum
      colloquium_forum_save($forum);
    }
  }

  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('colloquium_forum', $entity);

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}

class ColloquiumPostTypeController extends EntityAPIControllerExportable {
   public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'button_label' => 'Post',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save post Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    // Rebuild menu registry. We do not call menu_rebuild directly, but set
    // variable that indicates rebuild in the end.
    // @see http://drupal.org/node/1399618
    variable_set('menu_rebuild_needed', TRUE);
  }
}

/**
 * UI controller for Topic Type.
 */
class ColloquiumPostTypeUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults.
   */
  /*public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage topic types.';
    return $items;
  }*/
}