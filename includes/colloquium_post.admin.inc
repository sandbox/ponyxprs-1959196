<?php

/**
 * Generates the post type editing form.
 */
function colloquium_post_type_form($form, &$form_state, $post_type, $op = 'edit') {

  if ($op == 'clone') {
    $post_type->label .= ' (cloned)';
    $post_type->type = '';
  }
  
  //human readable name of the type
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $post_type->label,
    '#description' => t('The human-readable name of this post type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($post_type->type) ? $post_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $post_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'colloquium_post_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this post type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  
  //Label used on the button to create posts of the particular type
  $form['button_label'] = array(
    '#title' => t('Button Label'),
    '#type' => 'textfield',
    '#default_value' => $post_type->button_label,
    '#description' => t('The name of this type used for links and buttons.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  //description of the topic type
  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($post_type->description) ? $post_type->description : '',
    '#description' => t('Description about the post type.'),
  );
  
  //status of the entity type like custom, default, overwritten
  $form['status'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($post_type->status) ? $post_type->status : ENTITY_CUSTOM,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save post type'),
    '#weight' => 40,
  );

  if (!$post_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete post type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('colloquium_post_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing post_type.
 */
function colloquium_post_type_form_submit(&$form, &$form_state) {
  $post_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  colloquium_post_type_save($post_type);

  // Redirect user back to list of task types.
  $form_state['redirect'] = 'admin/structure/colloquium/post_types';
}

function colloquium_post_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/colloquium/post_types/' . $form_state['colloquium_post_type']->type . '/delete';
}
