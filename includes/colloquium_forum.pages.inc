<?php

/**
 * Forum view callback.
 */
function colloquium_forum_view($forum) {
  drupal_set_title(entity_label('colloquium_forum', $forum));
  return entity_view('colloquium_forum', array(entity_id('colloquium_forum', $forum) => $forum), 'full');
}