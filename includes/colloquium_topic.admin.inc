<?php

/**
 * Generates the topic type editing form.
 */
function colloquium_topic_type_form($form, &$form_state, $topic_type, $op = 'edit') {

  if ($op == 'clone') {
    $topic_type->label .= ' (cloned)';
    $topic_type->type = '';
  }
  //human readable name of the type
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $topic_type->label,
    '#description' => t('The human-readable name of this topic type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($topic_type->type) ? $topic_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $topic_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'colloquium_topic_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this topic type. It must only contain lowercase letters, numbers, and underscores.'),
  );
  
  //Label used on the button to create topics of the particular type
  $form['button_label'] = array(
    '#title' => t('Button Label'),
    '#type' => 'textfield',
    '#default_value' => $topic_type->button_label,
    '#description' => t('The name of this type used for links and buttons.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  
  $post_types = colloquium_post_types();
  $dropdown_values = array();
  
  foreach($post_types as $post_type) {
    $dropdown_values[$post_type->type] = $post_type->label;
  }
  
  //description of the topic type
  $form['post_type'] = array(
    '#title' => 'Post type',
    '#type' => 'select',
    '#key_type' => 'associative',
    '#options' => $dropdown_values,
  );

  //description of the thread type
  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($topic_type->description) ? $topic_type->description : '',
    '#description' => t('Description about the topic type.'),
  );
  
  //status of the entity type like custom, default, overwritten
  $form['status'] = array(
    '#type' => 'hidden',
    '#default_value' => isset($topic_type->status) ? $topic_type->status : ENTITY_CUSTOM,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save topic type'),
    '#weight' => 40,
  );

  if (!$topic_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete topic type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('colloquium_topic_type_form_submit_delete')
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing topic_type.
 */
function colloquium_topic_type_form_submit(&$form, &$form_state) {
  $topic_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  colloquium_topic_type_save($topic_type);

  // Redirect user back to list of task types.
  $form_state['redirect'] = 'admin/structure/colloquium/topic_types';
}

function colloquium_topic_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/colloquium/topic_types/' . $form_state['colloquium_topic_type']->type . '/delete';
}
